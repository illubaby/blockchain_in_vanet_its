# Blockchain in VANET and ITS

## Table of Contents
1. [Introduction](#introduction)
2. [Diagrams](#diagrams)
3. [Installation](#installation)
4. [Usage](#usage)
5. [Contributing](#contributing)
6. [License](#license)
7. [Acknowledgments](#acknowledgments)

## Introduction

This project aims to develop algorithms or innovative solutions for implementing Blockchain technology in both Vehicular Ad Hoc Networks (VANETs) and Intelligent Transportation Systems (ITS). The project is divided into two main branches, each represented by a diagram: one for Blockchain in VANETs and another for Blockchain in ITS.

## Diagrams

### Blockchain in VANETs
![Blockchain in VANETs Diagram](Blockchain_Vanet.md)

### Blockchain in ITS
![Blockchain in ITS Diagram](Blockchain_ITS.md)

## Installation

```bash
# Clone the repository
git clone https://gitlab.com/illubaby/blockchain_in_vanet_its

# Navigate to the project directory
cd blockchain-in-vanet-and-its

# Install dependencies
npm install
